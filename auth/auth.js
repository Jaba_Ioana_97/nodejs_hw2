require('dotenv').config();
const User = require('../model/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

exports.register = async(req, res) => {
    const { username, password } = req.body;
    // send the hashed password to database
    const saltRounds = 10;
    const salt = bcrypt.genSaltSync(saltRounds);
    const hash = bcrypt.hashSync(password, salt);
    await User.create({
      username,
      password: hash,
    })
    .then((user) => {
      const maxAge = 3 * 60 * 60;
      const token = jwt.sign(
        { user },
        process.env.ACCESS_TOKEN_SECRET,
        {
          expiresIn: maxAge, 
        }
      );
    res.status(200).json({
      message: 'User successfully created'
      });
    })
    .catch((err) =>
      res.status(400).json({
      message: 'User not successful created',
      error: err.message
      })
    );   
}

exports.login = async (req, res) => {
    const { username, password } = req.body;
    // check if username and password is provided
    if (!username || !password) {
      return res.status(400).json({
        message: 'Username or Password not present',
      });
    }

    try {
        const user = await User.findOne({ username });
        if (user === null) {
          res.status(400).json({
            message: 'Login not successful',
            error: 'User not found',
          })
        } else {
          bcrypt.compare(password, user.password).then((result) => {
            if(result) {
              const maxAge = 3 * 60 * 60;
              const token = jwt.sign(
                { user},
                process.env.ACCESS_TOKEN_SECRET,
                {
                  expiresIn: maxAge, 
                }
              );
              res.status(200).json({
                message: 'User successfully Logged in',
                jwt_token: token
              });
            } else {
              res.status(400).json({
                message: 'No match'
                })
              }
          })
        }
      } catch (err) {
        res.status(400).json({
          message: 'An error occured',
          error: err.message,
        })
    }
}

// verify Token
exports.verifyToken = (req, res, next) => {
    // get auth header value
    const bearerHeader = req.headers['authorization'];
    // check if bearer is undefined
    if(typeof bearerHeader !== 'undefined') {
        // split at the space 
        const bearer = bearerHeader.split(' ');
        // get token from array
        const bearerToken = bearer[1];
        // set the token 
        req.token = bearerToken;
        next();
    } else {
        // forbidden
        res.status(400).send('Forbidden');
    }
}