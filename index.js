require('dotenv').config();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('./model/user');
const Note = require('./model/note');

const express = require('express');
const { JsonWebTokenError } = require('jsonwebtoken');
const app = express();
const PORT = 8080;

const mongoose = require('mongoose');
const mongoString = process.env.DATABASE_URL;

mongoose.connect(mongoString);
const database = mongoose.connection;

database.on('error', (error) => {
    console.log('Error');
})

database.once('connected', () => {
    console.log('Database connected');
})

// middleware
app.use(express.json());

app.use((req, res, next) => {
    console.log('New request', `[${req.method}] ${req.url}`);
    next();
});

// authentication
app.use('/api/auth', require('./auth/route'));

// authorization
const { verifyToken } = require('./auth/auth');

app.get('/api/user/me', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
        if(err) {
            res.status(400).json({ 
                message: 'Error', 
                error: err.message 
            });
        } else {
            res.status(200).json({
                id: authData.user._id,
                username: authData.user.username,
                createDate: new Date()
            })
        }
    })
})

// delete personal account
app.delete('/api/user/me', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        if(err) {
            res.status(400).json({ message: 'Error' });
        } else {
            const id = authData.user._id;
            await User.findById(id)
            .then(user => user.remove())
            .then(user =>
                res.status(200).json({ 
                    message: 'User successfully deleted'
                })
            )
            .catch(err =>
                res.status(400).json({ 
                    message: 'An error occurred',
                    error: err.message
                })
            )
        }
    })
})

// change user's password
app.patch('/api/user/me', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        if(err) {
            res.status(400).json({ message: 'Error' });
        } else {
            const { oldPassword, newPassword } = req.body;
            const id = authData.user._id;
            const result = await bcrypt.compare(oldPassword, authData.user.password);
            if(result) {
                const saltRounds = 10;
                const salt = bcrypt.genSaltSync(saltRounds);
                const hash = bcrypt.hashSync(newPassword, salt);
                await User.findByIdAndUpdate(id, { "password": hash })
                .then(user => 
                    res.status(200).json({ 
                        message: 'Password updated'
                    })
                )
                .catch(err =>
                    res.status(400).json({ 
                        message: 'An error occurred',
                        error: err.message
                    })
                )
            } else {
                res.status(400).json({ 
                    message: `Password didn't match`
                })
            }
        }
    })
})

// create Note
app.post('/api/notes', verifyToken, (req, res) => {
    const { text } = req.body;
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        if(err) {
            res.status(400).json({ message: 'Error' });
        } else {
            const userId = authData.user._id;
            await Note.create({ text, userId, completed: true, createDate: new Date() })
                .then((note) => {
                    res.status(200).json({ 
                        message: 'Note created sccessfully'
                    });
                })
                .catch((err) => {
                    res.status(400).json({ 
                        message: 'An error occurred'
                    });
                })
        }
    })
})

// get user's notes
app.get('/api/notes', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        if(err) {
            res.status(400).json({ message: 'Error' });
        } else {
            const userId = authData.user._id;

            const offset = Number(req.query.offset);
            const limit = Number(req.query.limit);

            if(Number.isInteger(offset) === false || Number.isInteger(limit) === false) {
                return res.status(400).json({ 
                    message: 'Must be integer'
                });
            } else {
                const notes = await Note.find({ userId });
                notes.splice(0, offset);
                res.status(200).json({
                    offset: offset,
                    limit: limit,
                    count: notes.slice(0, limit).length,
                    notes:  notes.slice(0, limit)
                })
            }
        }
    })
})

// get user's note by id
app.get('/api/notes/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        if(err) {
            res.status(400).json({ message: 'Error' });
        } else {
            const id = req.params.id;
            await Note.findById(id)
            .then(note =>
                res.status(200).json({ 
                    message: note
                })
            )
            .catch(err =>
                res.status(400).json({ 
                    message: 'An error occurred'
                })
            )
        }
    })
})

// delete user's not by id
app.delete('/api/notes/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        if(err) {
            res.status(400).json({ message: 'Error' });
        } else {
            const id = req.params.id;
            await Note.findById(id)
            .then(note => {
                if(note.userId !== authData.user._id) {
                    res.status(400).send(`That note doesn't belong to that user`);
                } else {
                    note.remove();
                }
            })
            .then(note =>
                res.status(200).json({ 
                    message: `Successfully deleted note that belongs to ${authData.user.username}`
                })
            )
            .catch(err =>
                res.status(400).json({ 
                    message: 'An error occurred'
                })
            )
        }
    })
})

// update note by id
app.put('/api/notes/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        if(err) {
            res.status(400).json({ message: 'Error' });
        } else {
            const id = req.params.id;
            const newText = req.body.text;
            await Note.findByIdAndUpdate(id, {"text": newText})
            .then(note =>
                res.status(200).json({ 
                    message: 'Updated'
                })
            )
            .catch(err =>
                res.status(400).json({ 
                    message: 'An error occurred'
                })
            )
        }
    })
})

// patch note by id => change completed (true/ false) if the note is check/ uncheck
app.patch('/api/notes/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, async(err, authData) => {
        if(err) {
            res.status(400).json({ message: 'Error' });
        } else {
            const id = req.params.id;
            await Note.findByIdAndUpdate(id, [{ $set:{ completed:{ $not: "$completed" } } } ])
            .then(note => {
                if(note.completed === true) {
                    res.status(200).json({
                        message: "Uncheck"
                        })
                } else {
                    res.status(200).json({
                            message: "Check"
                    })
                }
            })
            .catch(err =>
                res.status(400).json({ 
                    message: 'An error occurred'
                })
            )
        }
    })
})

// start the server
app.listen(PORT, (err) => {
    if (err) {
      console.log('Error during server starting');
      return;
    }
    console.log(`Server Started at ${PORT}`);
});

// Resources
// https://www.youtube.com/watch?v=mbsmsi7l3r4
// https://blog.logrocket.com/building-structuring-node-js-mvc-application/
// https://dev.to/eetukudo_/understanding-mvc-pattern-in-nodejs-2bdn
// https://www.mongodb.com/languages/express-mongodb-rest-api-tutorial
// https://www.freecodecamp.org/news/build-a-restful-api-using-node-express-and-mongodb/
// https://stackoverflow.com/questions/65305856/no-write-concern-mode-named-majority-found-in-replica-set-configuration-err
// https://www.youtube.com/watch?v=mbsmsi7l3r4
// https://www.youtube.com/watch?v=7nafaH9SddU
// https://www.loginradius.com/blog/engineering/guest-post/nodejs-authentication-guide/
// https://kb.objectrocket.com/mongo-db/how-to-use-mongoose-to-find-by-id-and-update-with-an-example-1209
// https://www.appsloveworld.com/mongodb/100/31/toggle-a-boolean-value-with-mongodb