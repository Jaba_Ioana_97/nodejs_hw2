const Mongoose = require('mongoose');

const NoteSchema = new Mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    createDate: {
        type: String,
        required: true
    }
});

const Note = Mongoose.model('note', NoteSchema);
module.exports = Note;